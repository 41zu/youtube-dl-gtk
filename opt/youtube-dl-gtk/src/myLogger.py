# DEBUG: Detailed information, typically of interest only when diagnosing problems.

# INFO: Confirmation that things are working as expected.

# WARNING: An indication that something unexpected happened, or indicative of some problem in the near future (e.g. ‘disk space low’). The software is still working as expected.

# ERROR: Due to a more serious problem, the software has not been able to perform some function.

# CRITICAL: A serious error, indicating that the program itself may be unable to continue running.

import logging

class NoneLogger:
    def debug(self, msg):
        pass

    def info(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        pass

    def critical(self, msg):
        pass

class TerminalLogger:
    def debug(self, msg):
        print('Debug: ', msg)

    def info(self, msg):
        print('Info: ', msg)

    def warning(self, msg):
        print('Warning: ', msg)

    def error(self, msg):
        print('Error: ', msg)

    def critical(self, msg):
        print('Critical: ', msg)

class FileLogger:
    def __init__(self, save_dir):
        logging.basicConfig(filename=save_dir + '/youtube-dl-gtk.log', level=logging.DEBUG, format='%(asctime)s [%(levelname)s] %(message)s')

    def debug(self, msg):
        logging.debug(msg)

    def info(self, msg):
        logging.info(msg)

    def warning(self, msg):
        logging.warning(msg)

    def error(self, msg):
        logging.error(msg)

    def critical(self, msg):
        logging.critical(msg)

if __name__ == '__main__':
    pass
