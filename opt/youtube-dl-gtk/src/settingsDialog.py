import gi # for Gtk
import re # regex lib
import os # for home dir

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from i18n import t

dialog = None
settings = None

# python class syntax https://www.geeksforgeeks.org/constructors-in-python/
# gtk layout containers https://python-gtk-3-tutorial.readthedocs.io/en/latest/layout.html
class SettingsDialog():

    def __init__(self, main_window, gsettings, EXEC_DIR):
        global dialog
        global settings
        settings = gsettings # We set the global variable, which we can than use in the whole module

        # Make an GTK Builder object
        builder = Gtk.Builder()
        # Import the glade file with the layout of the GUI
        builder.add_from_file(EXEC_DIR + '/glade/settings_dialog.glade')

        # Connect the the handler methods in the class
        builder.connect_signals(SettingsSignalHandler())

        # Set the dialog objekt, the old one gets replaced
        dialog = builder.get_object('dialog_settings')

        # This allows window managers to e.g. keep the dialog on top of the main window
        dialog.set_transient_for(main_window)

        # Set the dialog title
        dialog.set_title(t('settings_title'))

        # Set the response buttons into the dialog
        # We do it manually, so we can easily connect the ResponseType to it
        dialog.add_button(Gtk.STOCK_DISCARD, Gtk.ResponseType.CANCEL)
        dialog.add_button(Gtk.STOCK_APPLY, Gtk.ResponseType.APPLY)

        # Create the list boxes for each settings page
        # Create for each settings page one list, with the box_ids, which are defined in the settings glade file
        settings_general = ['box_settings_only_sound', 'box_settings_notify_when_finished', 'box_settings_check_url_with_youtube_regex']
        settings_subtitles = ['box_settings_download_subtitles', 'box_settings_show_subtitles_availability', 'box_settings_languages']
        settings_logger = ['box_settings_logger_active', 'box_settings_logger_type', 'box_settings_logger_use_own_dir', 'box_settings_logger_own_dir_path']

        # Iterate through the box list with the ids and add the rows into the ListBox
        listbox_general = Gtk.ListBox(visible=True, valign=Gtk.Align.CENTER, selection_mode=Gtk.SelectionMode.NONE)
        listbox_general.set_border_width(50)
        for box_id in settings_general:
            row = Gtk.ListBoxRow(visible=True, margin=2)
            row.add(builder.get_object(box_id))
            listbox_general.add(row)
        
        listbox_subtitles = Gtk.ListBox(visible=True, valign=Gtk.Align.CENTER, selection_mode=Gtk.SelectionMode.NONE)
        listbox_subtitles.set_border_width(50)
        for box_id in settings_subtitles:
            row = Gtk.ListBoxRow(visible=True, margin=2)
            row.add(builder.get_object(box_id))
            listbox_subtitles.add(row)

        listbox_logger = Gtk.ListBox(visible=True, valign=Gtk.Align.CENTER, selection_mode=Gtk.SelectionMode.NONE)
        listbox_logger.set_border_width(50)
        for box_id in settings_logger:
            row = Gtk.ListBoxRow(visible=True, margin=2)
            row.add(builder.get_object(box_id))
            listbox_logger.add(row)

        # Create the Stack with the StackSwitcher
        box_settings = builder.get_object('box_settings')
        stack = Gtk.Stack(visible=True)
        stack.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
        stack.set_transition_duration(400)
        stack.add_titled(listbox_general, 'general', t('label_settings_stack_title_general'))
        stack.add_titled(listbox_subtitles, 'subtitles', t('label_settings_stack_title_subtitles'))
        stack.add_titled(listbox_logger, 'logger', t('label_settings_stack_title_logger'))
        stack_switcher = Gtk.StackSwitcher(visible=True, halign=Gtk.Align.CENTER)
        stack_switcher.set_stack(stack)
        box_settings.pack_start(stack_switcher, True, True, 0)
        box_settings.pack_start(stack, True, True, 0)

        # Load the settings widgets
        # General tab
        dialog.swtich_settings_only_sound = builder.get_object('swtich_settings_only_sound')
        dialog.switch_settings_notify_when_finished = builder.get_object('switch_settings_notify_when_finished')
        dialog.switch_settings_check_url_with_youtube_regex = builder.get_object('switch_settings_check_url_with_youtube_regex')
        # Subtitles tab
        dialog.switch_download_subtitles = builder.get_object('switch_download_subtitles')
        dialog.switch_show_subtitles_availability = builder.get_object('switch_show_subtitles_availability')
        dialog.entry_languages = builder.get_object('entry_languages')
        dialog.label_settings_languages_entry_indicator = builder.get_object('label_settings_languages_entry_indicator')
        # Logger tab
        dialog.switch_settings_logger_active = builder.get_object('switch_settings_logger_active')
        dialog.radiobutton_settings_logger_type_file = builder.get_object('radiobutton_settings_logger_type_file')
        dialog.radiobutton_settings_logger_type_cli =  builder.get_object('radiobutton_settings_logger_type_cli')
        dialog.switch_settings_logger_use_own_dir =    builder.get_object('switch_settings_logger_use_own_dir')
        dialog.button_settings_logger_own_dir_path =   builder.get_object('button_settings_logger_own_dir_path')

        # Set the translations into the labels
        # General tab
        builder.get_object('label_settings_only_sound').set_text(t('label_settings_only_sound'))
        builder.get_object('label_settings_notify_when_finished').set_text(t('label_settings_notify_when_finished'))
        builder.get_object('label_settings_check_url_with_youtube_regex').set_text(t('label_settings_check_url_with_youtube_regex'))
        # Subtitles tab
        builder.get_object('label_settings_download_subtitles').set_text(t('label_settings_download_subtitles'))
        builder.get_object('label_settings_show_subtitles_availability').set_text(t('label_settings_show_subtitles_availability'))
        builder.get_object('label_settings_languages').set_text(t('label_settings_languages'))
        # Logger tab
        builder.get_object('label_settings_logger_active').set_text(t('label_settings_logger_active'))
        builder.get_object('label_settings_logger_type').set_text(t('label_settings_logger_type'))
        dialog.radiobutton_settings_logger_type_file.set_label(t('radiobutton_settings_logger_type_file'))
        dialog.radiobutton_settings_logger_type_cli.set_label(t('radiobutton_settings_logger_type_cli'))
        builder.get_object('label_settings_logger_use_own_dir').set_text(t('label_settings_logger_use_own_dir'))
        builder.get_object('label_settings_logger_own_dir_path').set_text(t('label_settings_logger_own_dir_path'))
        builder.get_object('label_settings_logger_own_dir_path_info').set_text(t('label_settings_logger_own_dir_path_info'))

        # Load the settings values from gsettings and set them into the gui
        # General tab
        dialog.swtich_settings_only_sound.set_active(settings.get_boolean('only-sound'))
        dialog.switch_settings_notify_when_finished.set_active(settings.get_boolean('notify-when-finished'))
        dialog.switch_settings_check_url_with_youtube_regex.set_active(settings.get_boolean('check-url'))
        # Subtitles tab
        dialog.switch_download_subtitles.set_active(settings.get_boolean('download-subtitles'))
        dialog.switch_show_subtitles_availability.set_active(settings.get_boolean('show-subtitles-availability'))
        dialog.entry_languages.set_text(settings.get_string('subtitles-languages'))
        if dialog.switch_download_subtitles.get_active() == False:
            dialog.switch_show_subtitles_availability.set_sensitive(False)
            dialog.entry_languages.set_sensitive(False)
        # Logger tab
        dialog.switch_settings_logger_active.set_active(settings.get_boolean('logger-active'))
        # Logger tab: set radio buttons
        logger_type = settings.get_string('logger-type')
        if logger_type == 'file': # set the radio buttons in the logger tab
            dialog.radiobutton_settings_logger_type_file.set_active(True)
        elif logger_type == 'cli':
            dialog.radiobutton_settings_logger_type_cli.set_active(True)
        dialog.switch_settings_logger_use_own_dir.set_active(settings.get_boolean('logger-use-own-dir'))
        # Logger tab: set the path into the button, if there is still no value saved, we set the home dir
        log_file_path = settings.get_string('logger-own-dir-path')
        if log_file_path == '':
            log_file_path = os.path.expanduser('~')
        dialog.button_settings_logger_own_dir_path.set_label(log_file_path)
        if dialog.switch_settings_logger_active.get_active() == False:
            dialog.radiobutton_settings_logger_type_file.set_sensitive(False)
            dialog.radiobutton_settings_logger_type_cli.set_sensitive(False)
            dialog.switch_settings_logger_use_own_dir.set_sensitive(False)
            dialog.button_settings_logger_own_dir_path.set_sensitive(False)
        if dialog.switch_settings_logger_use_own_dir.get_active() == False:
            dialog.button_settings_logger_own_dir_path.set_sensitive(False)

    def run(self):
        global dialog

        response = dialog.run()

        # If the user presses the apply button, we save the settings
        # If the user does anything else, do nothing
        if (response == Gtk.ResponseType.APPLY):
            self.__apply_settings__()

        # We destroy the dialog (which also closes the window) and set the module vars to None, so no weird things can happen
        dialog.destroy()
        dialog = None
        settings = None

    def __apply_settings__(self):
        # General tab
        settings.set_boolean('only-sound', dialog.swtich_settings_only_sound.get_active())
        settings.set_boolean('notify-when-finished', dialog.switch_settings_notify_when_finished.get_active())
        settings.set_boolean('check-url', dialog.switch_settings_check_url_with_youtube_regex.get_active())
        # Subtitles tab
        settings.set_boolean('download-subtitles', dialog.switch_download_subtitles.get_active())
        settings.set_boolean('show-subtitles-availability', dialog.switch_show_subtitles_availability.get_active())
        # If the input in the entry passes the regex, we save it
        entry_languages_text = dialog.entry_languages.get_text()
        if check_language_entry(entry_languages_text):
            settings.set_string('subtitles-languages', entry_languages_text)
        # Logger tab
        settings.set_boolean('logger-active', dialog.switch_settings_logger_active.get_active())
        if dialog.radiobutton_settings_logger_type_file.get_active() == True:
            settings.set_string('logger-type', 'file')
        elif dialog.radiobutton_settings_logger_type_cli.get_active() == True:
            settings.set_string('logger-type', 'cli')
        settings.set_boolean('logger-use-own-dir', dialog.switch_settings_logger_use_own_dir.get_active())
        settings.set_string('logger-own-dir-path', dialog.button_settings_logger_own_dir_path.get_label())

class SettingsSignalHandler(Gtk.Dialog):
    def switch_download_subtitles_state_changed(self, switch, state):
        dialog.switch_show_subtitles_availability.set_sensitive(state)
        dialog.entry_languages.set_sensitive(state)

        # The default signal handler should still run
        # True would mean, that we don't want to run it
        return False

    def switch_logger_active_state_changed(self, switch, state):
        dialog.radiobutton_settings_logger_type_file.set_sensitive(state)
        dialog.radiobutton_settings_logger_type_cli.set_sensitive(state)
        dialog.switch_settings_logger_use_own_dir.set_sensitive(state)
        # don't set the path button sensitive, when the corresponding switch is not active
        if dialog.switch_settings_logger_use_own_dir.get_active():
            dialog.button_settings_logger_own_dir_path.set_sensitive(state)

    def switch_logger_use_own_dir_state_changed(self, switch, state):
        dialog.button_settings_logger_own_dir_path.set_sensitive(state)

    def entry_languages_changed(self, entry):
        if not check_language_entry(entry.get_text()):
            entry.get_style_context().add_class(Gtk.STYLE_CLASS_DESTRUCTIVE_ACTION)
            dialog.label_settings_languages_entry_indicator.set_text('❌')
        else:
            entry.get_style_context().remove_class(Gtk.STYLE_CLASS_DESTRUCTIVE_ACTION)
            dialog.label_settings_languages_entry_indicator.set_text('✅')

    def button_logger_logfile_path_clicked(self, button):
        dir_choose_dialog = Gtk.FileChooserDialog(action=Gtk.FileChooserAction.SELECT_FOLDER, parent=self, title=t('dialog_error_to_file'))
        dir_choose_dialog.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK)
        dir_choose_dialog.set_current_folder(button.get_label())
        response = dir_choose_dialog.run()
        if response == Gtk.ResponseType.OK:
            button.set_label(dir_choose_dialog.get_filename())
        dir_choose_dialog.destroy()

def check_language_entry(text) -> bool:
        lang_regex = r'(^([a-z]{2})?(,[a-z]{2})*$)'
        match = re.match(lang_regex, text)
        if match:
            return True
        else:
            return False

if __name__ == '__main__':
    pass
