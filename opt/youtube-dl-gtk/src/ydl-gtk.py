#!/usr/bin/python3

# How to profile
# Install 'line-profiler' with pip
# use @profile decorator for the methods you want to profile
# run program with 'kernprof -l script_to_profile.py'
# show the results in the terminal with 'python -m line_profiler script_to_profile.py.lprof'

# Test Videos
# https://www.youtube.com/watch?v=-R307w05iJc Warning:  de subtitles not available for -R307w05iJc
# https://www.youtube.com/watch?v=A1x7FqXRy9c Ted-Ed video with a lot of subtitles
# https://www.youtube.com/watch?v=ek0FYB9zXJw short test video

#FIXME:
#TODO: 1. make use of the logger class in the whole program 2. add a possibility to abort the download 3. check if we can change the german locale file from de_DE to just de

# python modules
import re # regex lib
import os # for exec dir and user dir
import csv # for seperating the country codes
import time
import traceback # traceback for exceptions
import threading # needed for multithreading
from datetime import datetime


import yt_dlp # for youtube_dl                          #PyPI: yt-ydp
import i18n # translates                                #PyPI: python-i18n
import humanize # formating into human readable text    #Pypi: humanize
import locale # to read your system language
import gi # for Gtk and all associated dependencies

gi.require_version('Gtk', '3.0')
gi.require_version('Notify', '0.7')
from gi.repository import Gtk, Gdk, GdkPixbuf, GLib, Gio, Pango, Notify
from i18n import t

# own modules
import myLogger
from settingsDialog import SettingsDialog

# global constants
VERSION = '3.0'
EXEC_DIR = os.path.abspath(os.path.dirname(__file__)) # set the dir var as the location, where the scirpt resides
PIXMAPS_DIR = EXEC_DIR + '/../pixmaps/'
APPLICATION_ID = 'mueka.dominik.YdlGtk'

# global var
window = None
save_dir = None # gets set later
traceback_str = None
downloading_bool = False
postprocessing_bool = False
subtitles_list = []
ydl_opts = {}

# Here gets the GSettings-Objekt initialized
schema_source = Gio.SettingsSchemaSource.new_from_directory(EXEC_DIR + '/schemas', Gio.SettingsSchemaSource.get_default(), False)
schema = Gio.SettingsSchemaSource.lookup(schema_source, 'com.gitlab.41zu.youtube.dl.gtk', False)
settings = Gio.Settings.new_full(schema, None, None)

# On the first startup of the app, the variable in dconf gets initialiszed with an empty string
if settings.get_string('path-save-folder') == '':
    settings.set_string('path-save-folder', os.path.expanduser('~')) # set it to the home dir
save_dir = settings.get_string('path-save-folder') # always set the var

# configure i18n
# https://pypi.org/project/python-i18n/
i18n.load_path.append(EXEC_DIR + '/../locale')
i18n.set('filename_format', '{locale}.{format}')
i18n.set('locale', locale.getlocale()[0])
i18n.set('fallback', 'en')
#i18n.set('locale', 'jp_JP') # Test other locale, default english

# All signal handler methods come in this sub class
# (Same Thread as GTK, so no GLib needed :)
class SignalHandler(Gtk.Window):
    # checks if the string is a valid Youtube-URL
    # and starts the downloading in a new thread
    def button_search_clicked(self, button):
        adress = window.entry_adress.get_text().strip()
        # Go into the if, when either the url is a youtube adress or when the check is disabled
        if youtube_url_validation(adress) or not settings.get_boolean('check-url'):
            window.infobar.set_revealed(False)
            tmp_dir = window.entry_location.get_text().strip()
            if if_path_valid_save_dir(tmp_dir): # the error gets set in the method
                window.entry_location.set_text(tmp_dir) # set the stripped path into the entry
                thread = MyThread(adress)
                thread.daemon = True # we set it as an daemon thread so it will exit, if we end the programm
                thread.start()
        else:
            infobar_show_error(t('infobar_invalid_address'), False)

    # Opens a folder choose dialog and sets the save_dir string
    def button_choose_location_clicked(self, button):
        global save_dir
        global window

        dialog = Gtk.FileChooserDialog(
            title=t('dialog_folder_title'),
            parent=self,
            action=Gtk.FileChooserAction.SELECT_FOLDER,
        )
        dialog.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, t('dialog_folder_button_ok'), Gtk.ResponseType.OK)
        dialog.set_current_folder(save_dir)

        #dialog.set_default_size(800, 400)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            save_dir = dialog.get_filename()
            window.entry_location.set_text(save_dir)
            settings.set_string('path-save-folder', save_dir)

        dialog.destroy()

    def button_open_settings_clicked(self, button):
        dialog = SettingsDialog(window, settings, EXEC_DIR)
        dialog.run()

    def button_show_error_clicked(self, button):
        global traceback_str

        # Make an GTK Builder object
        builder = Gtk.Builder()
        # Import the glade file with the layout of the GUI
        builder.add_from_file(EXEC_DIR + '/glade/scrollable_text_dialog.glade')

        dialog = builder.get_object('dialog_scrollable_text')
        builder.get_object('label_scrollable_text').set_text(traceback_str)

        # This allows window managers to e.g. keep the dialog on top of the main window
        dialog.set_transient_for(self)
        dialog.set_title(t('dialog_error_title'))
        dialog.add_buttons(t('dialog_error_to_file'), Gtk.ResponseType.ACCEPT, t('dialog_error_copy'), Gtk.ResponseType.APPLY, Gtk.STOCK_CLOSE, Gtk.ResponseType.CLOSE)
        display = Gdk.Display.get_default()
        if display is None:
            # if Gdk.Display.get_default() return None, there will be no clipboard, thats why desensitize the copy button
            dialog.set_response_sensitive(Gtk.ResponseType.APPLY, False)
        response = dialog.run()
        try:
            if response == Gtk.ResponseType.ACCEPT:
                save_dialog = Gtk.FileChooserDialog(action=Gtk.FileChooserAction.SAVE, parent=self, title=t('dialog_error_to_file'))
                save_dialog.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_SAVE, Gtk.ResponseType.OK)
                save_dialog.set_current_folder(os.path.expanduser('~'))
                save_dialog.set_current_name('youtube-dl-gtk_error_details_' + str(datetime.now()) + '.txt')
                save_dialog.set_do_overwrite_confirmation(True)
                if save_dialog.run() == Gtk.ResponseType.OK:
                    f = open(save_dialog.get_filename(), 'w')
                    f.write(traceback_str)
                    f.close()
                save_dialog.destroy()
            elif response == Gtk.ResponseType.APPLY:
                clipboard = Gtk.Clipboard.get_default(display)
                clipboard.set_text(traceback_str, -1)
        except:
            print(traceback.format_exc())
        # at the end we destroy the dialog, because, when we destroy both dialog before the other is opened, it doesn't look so good
        dialog.destroy()

    def infobar_button_close_clicked(self, info_bar, response_id):
        infobar_close()
        #we still need to resolve the artifacts after closing the infobar (only on some graphic cards)
        #window.queue_draw()

    # https://stackoverflow.com/questions/36921706/gtk3-ask-confirmation-before-application-quit
    def application_delete_event(self, event, what):
        if not downloading_bool:
            uninitialize_notify() # When we quit the dialog, unitialize notify first
            return False # We quit the program without an dialog

        exit_dialog = Gtk.MessageDialog(
            parent=self,
            title=t('dialog_application_quit_title'),
            text=t('dialog_application_quit_text')
        )
        exit_dialog.add_buttons(Gtk.STOCK_YES, Gtk.ResponseType.YES, Gtk.STOCK_NO, Gtk.ResponseType.NO)
        response = exit_dialog.run()
        exit_dialog.destroy()
        if response == Gtk.ResponseType.NO:
            return True # stop other handlers from being invoked for the event
        # Otherwise we quit the programm
        uninitialize_notify()
        return False

# The multithreading sub class with is inheriting from 'Thread'
class MyThread(threading.Thread):
    # The init method wich also adds the url attribute to 'self'
    def __init__(self, url):
        threading.Thread.__init__(self)
        self.url = url

    # Is called after init, starts the downloading in the new thread
    def run(self):
        global window
        global traceback_str
        #print('##### Thread: ', threading.get_ident(), ' Location: myThread run()') #For threading tests
        set_options_to_ydl_dict()
        ydl_opts['logger'].info('VIDEO URL: {}'.format(self.url)) # logging
        with yt_dlp.YoutubeDL(ydl_opts) as ydl:
            try:
                upadte_gui_when_starting()
                setup_gui()
                result = ydl.extract_info(self.url, download=False)
                ydl.sanitize_info(result)

                if settings.get_boolean('download-subtitles'):
                    add_subtitles_as_labels(result)
                else:
                    # If the settings is false, set the frames visibility to false anyway, because it still could be visible from a earlier download
                    GLib.idle_add(update_gui_obj, window.frame_subtitles.set_visible, False)

                add_video_info_as_labels(result)
                ydl.download([self.url])
                # needs to be here, because if it where in update_gui_when_finished, it would also set it, if there was a error
                GLib.idle_add(update_gui_obj, window.label_status.set_text, t('label_status_finished'))
                # If the setting is activated, send a system notification, that the video is successfully downloaded
                if settings.get_boolean('notify-when-finished'):
                    send_notification(t('notification_download_success').format(result['title'])) # -> Title successfully downloaded
            except:
                GLib.idle_add(update_gui_obj, window.label_status.set_text, '') # set the label empty
                traceback_str = traceback.format_exc()
                infobar_show_error(t('infobar_download_error'), True)
        update_gui_when_finished()

def set_options_to_ydl_dict():
    ydl_opts.clear()
    # settings we always set
    ydl_opts['progress_hooks'] = [progress_hook]
    ydl_opts['noplaylist'] = True
    ydl_opts['paths'] = {'home': save_dir}
    # set different options before downloading the video
    if settings.get_boolean('only-sound'):
        ydl_opts['format'] = 'bestaudio/best'
        ydl_opts['postprocessors'] = [{'key': 'FFmpegExtractAudio', 'preferredcodec': 'mp3'}]
    else:
        ydl_opts['writesubtitles'] = True
        ydl_opts['writeautomaticsub'] = True
        ydl_opts['subtitleslangs'] = subtitles_list
        ydl_opts['postprocessors'] = [{'key':'FFmpegEmbedSubtitle'}]
    # set which logger should be used
    if settings.get_boolean('logger-active'):
        logger_type = settings.get_string('logger-type')
        if logger_type == 'file':
            if settings.get_boolean('logger-use-own-dir'):
                ydl_opts['logger'] = myLogger.FileLogger(settings.get_string('logger-own-dir-path'))
            else:
                ydl_opts['logger'] = myLogger.FileLogger(save_dir)
        elif logger_type == 'cli':
            ydl_opts['logger'] = myLogger.TerminalLogger()
        else:
            ydl_opts['logger'] = myLogger.NoneLogger()
    else:
        ydl_opts['logger'] = myLogger.NoneLogger()

def update_gui_obj(obj, val):
    if val != None:
        obj(val)
    else:
        obj()
    return False

# if the download of the video starts, setup the gui here
def setup_gui():
    GLib.idle_add(update_gui_obj, window.label_status.set_text, t('label_status_downloading'))
    if not window.progress_bar_download.get_visible():
        GLib.idle_add(update_gui_obj, window.progress_bar_download.set_visible, True)

def upadte_gui_when_starting():
    global downloading_bool
    downloading_bool = True # we set this bool, so when the user tries to quit the program, a dialog will show up
    GLib.idle_add(update_gui_obj, window.button_search_adress.set_sensitive, False)
    GLib.idle_add(update_gui_obj, window.entry_location.set_sensitive, False)
    GLib.idle_add(update_gui_obj, window.button_choose_location.set_sensitive, False)

# when the download is finished, update the gui so you can start and download again, also reset some stuff
def update_gui_when_finished():
    global downloading_bool
    downloading_bool = False
    global postprocessing_bool
    postprocessing_bool = False
    GLib.idle_add(update_gui_obj, window.button_search_adress.set_sensitive, True)
    GLib.idle_add(update_gui_obj, window.entry_location.set_sensitive, True)
    GLib.idle_add(update_gui_obj, window.button_choose_location.set_sensitive, True)
    GLib.idle_add(update_gui_obj, window.label_time_remaining.set_text, '')
    GLib.idle_add(update_gui_obj, window.label_download_speed.set_text, '')

def add_subtitles_as_labels(result):
    no_subtitles_for_list = []
    # There can be entries from the last download, that's why we clear it
    subtitles_list.clear()
    country_codes = settings.get_string('subtitles-languages')
    reader = csv.reader(country_codes.split('\n'), delimiter=',')
    for codes in reader:
        for code in codes:
            added = False
            if (code != ''):
                if 'subtitles' in result:
                    if code in result['subtitles']:
                        subtitles_list.append(code)
                        added = True
                # If the subtitle was not found, ckeck for it again here
                if 'automatic_captions' in result and not added:
                    if code in result['automatic_captions']:
                        subtitles_list.append(code)
                        added = True
                if not added:
                    # If there are nether subtitles nor automatic captions aviable for this subtitle, just add it to the unaviable list
                    no_subtitles_for_list.append(code)
    # Only show subtitles aviability, if the settings is turned on
    if settings.get_boolean('show-subtitles-availability'):
        # Remove all childen from the previous video result
        GLib.idle_add(update_gui_obj, window.box_subtitles.foreach, lambda child: child.destroy())
        # Add the aviable and not aviable subtitles as labels to the subtitles frame
        for code in subtitles_list:
            GLib.idle_add(update_gui_obj, window.box_subtitles.add, Gtk.Label(label='✅ ' + code, visible=True, halign=Gtk.Align.START))
        for code in no_subtitles_for_list:
            GLib.idle_add(update_gui_obj, window.box_subtitles.add, Gtk.Label(label='❌ ' + code, visible=True, halign=Gtk.Align.START))
        GLib.idle_add(update_gui_obj, window.frame_subtitles.set_visible, True)
    else:
        GLib.idle_add(update_gui_obj, window.frame_subtitles.set_visible, False)

def add_video_info_as_labels(result):
    # Remove all childen from the previous video result
    GLib.idle_add(update_gui_obj, window.box_video_info.foreach, lambda child: child.destroy())
    # Add all aviable information about the video as labels to the video info frame
    for key in ['title', 'uploader', 'duration_string', 'resolution']:
        if key in result:
            hbox = Gtk.HBox(halign=Gtk.Align.START, spacing=10, visible=True)
            hbox.add(Gtk.Label(label='<b>' + t('label_video_info_' + key) + ':</b>', use_markup=True, visible=True)) #bold label
            hbox.add(Gtk.Label(label=result[key], ellipsize=Pango.EllipsizeMode.END, visible=True))
            GLib.idle_add(update_gui_obj, window.box_video_info.add, hbox)
    # We only need for the filesize an extra if statement, because we need to 'humanize'.
    # It was observed, that 'filesize_approx' was set, but was None.
    if 'filesize_approx' in result and result['filesize_approx'] is not None:
        hbox = Gtk.HBox(halign=Gtk.Align.START, spacing=10, visible=True)
        hbox.add(Gtk.Label(label='<b>' + t('label_video_info_filesize') + ':</b>', use_markup=True, visible=True)) #bold label
        hbox.add(Gtk.Label(label=humanize.naturalsize(result['filesize_approx']), visible=True))
        GLib.idle_add(update_gui_obj, window.box_video_info.add, hbox)
    GLib.idle_add(update_gui_obj, window.frame_video_info.set_visible, True)

def if_path_valid_save_dir(tmp_dir: str) -> bool:
    global save_dir
    global traceback_str

    # Check if the saving location changed
    if save_dir == tmp_dir:
        return True
    else:
        if not os.path.isabs(tmp_dir):
            infobar_show_error(t('infobar_not_absolute_path').format(os.path.expanduser('~')), False)
            return False
        if os.path.exists(tmp_dir):
            if os.path.isdir(tmp_dir):
                save_dir = tmp_dir # Set the global var
                settings.set_string('path-save-folder', save_dir)
                return True
            else:
                infobar_show_error(t('infobar_path_is_file'), False)
                return False
        else:
            try:
                os.makedirs(tmp_dir)
                save_dir = tmp_dir # Set the global var
                settings.set_string('path-save-folder', save_dir)
                return True
            except:
                traceback_str = traceback.format_exc()
                infobar_show_error(t('infobar_invalid_path'), True)
                return False

# Sends an system notification
def send_notification(message: str):
    # First we try to init Notify
    if not Notify.is_initted():
        Notify.init('youtube-dl-gtk') # The name or id of the application initializing libnotify

    # If it successfully initialized, it will push a notification
    if Notify.is_initted():
        notification = Notify.Notification.new(t('window_title'), message, None)
        notification.set_image_from_pixbuf(GdkPixbuf.Pixbuf().new_from_file(PIXMAPS_DIR + 'youtube-dl-gtk.png'))
        notification.show()

# This method uninizializes Notify
# This method gets called in the application_delete_event-Method
def uninitialize_notify():
    if Notify.is_initted():
        Notify.uninit()

# Youtube_dl regularly gives updates to the download progress.
# We update the GUI from here
# (Same Thread as myThread, that's why use GLib!)
def progress_hook(d):
    if d['status'] == 'downloading':
        valstr = d.get('_percent_str')
        valstr = valstr.replace('%','')
        # '_percent_str' can also be the value 'Unknown %', which would fail to parse
        try:
            valfl = float(valstr)
            valfl = valfl / 100
            GLib.idle_add(update_gui_obj, window.progress_bar_download.set_fraction, valfl)
        except:
            pass

        # It was observed, that these strings can contain terminal formatting, that's why we remove it.
        GLib.idle_add(update_gui_obj, window.label_time_remaining.set_text, remove_terminal_formatting('ETA ' + d.get('_eta_str')))
        GLib.idle_add(update_gui_obj, window.label_download_speed.set_text, remove_terminal_formatting(d.get('_speed_str')))
    elif d['status'] == 'finished':
        GLib.idle_add(update_gui_obj, window.progress_bar_download.set_fraction, 1.0)
        if d['info_dict']['ext'] != 'webm':
            return

        if d['info_dict']['acodec'] != 'none':
            global postprocessing_bool
            postprocessing_bool = True
            threading.Thread(target=keep_progress_bar_pulse, args=()).start()
            GLib.idle_add(update_gui_obj, window.label_status.set_text, t('label_status_postprocessing'))
    elif d['status'] == 'error':
        # is handled by the try in run(self)
        #infobar_show_error(t('infobar_download_error'), False)
        pass

def remove_terminal_formatting(text: str) -> str:
    return re.sub(r'\033\[[0-9;]*m', '', text)

def infobar_show_error(message, bool_show_error_button):
    GLib.idle_add(update_gui_obj, window.infobar.set_message_type, Gtk.MessageType.ERROR)
    GLib.idle_add(update_gui_obj, window.infobar.label_error.set_label, message)
    if traceback_str is not None:
        GLib.idle_add(update_gui_obj, window.infobar.button_show_error.set_visible, bool_show_error_button)
    if not window.infobar.get_revealed():
        GLib.idle_add(update_gui_obj, window.infobar.set_revealed, True)

def infobar_close():
    GLib.idle_add(update_gui_obj, window.infobar.set_revealed, False)
    global traceback_str
    traceback_str = None

def youtube_url_validation(url: str) -> bool:
    youtube_regex = (
        r'(https?://)?(www\.)?'
        r'(youtube|youtu|youtube-nocookie)\.(com|be)/'
        r'(watch\?v=|embed/|v/|.+\?v=)?([^&=%\?]{11})')

    youtube_regex_match = re.match(youtube_regex, url)
    if youtube_regex_match:
        return True
    else:
        return False

# Gets called in a new thread
def keep_progress_bar_pulse():
    while postprocessing_bool:
        GLib.idle_add(update_gui_obj, window.progress_bar_download.pulse, None)
        time.sleep(0.05)
    # When the pulsing stops, we set the progress to 100%
    GLib.idle_add(update_gui_obj, window.progress_bar_download.set_fraction, 1.0)

# The GUI gets initilized here when the programm starts
def initialize(app):
    # Make an GTK Builder object
    builder = Gtk.Builder()
    # Import the glade file with the layout of the GUI
    builder.add_from_file(EXEC_DIR + '/glade/main_window.glade')
    # Connect the the signal handler methods in the class
    builder.connect_signals(SignalHandler())

    # Import the global window objekt
    global window
    # Set the window objekt
    window = builder.get_object('main_window')
    window.set_application(app)
    window.set_icon_name('youtube')

    # Set the namespaces in the window objekt
    window.entry_adress = builder.get_object('entry_adress')
    window.progress_bar_download = builder.get_object('progress_bar_download')
    window.infobar = builder.get_object('infobar')
    window.infobar.label_error = builder.get_object('label_error')
    window.infobar.button_show_error = builder.get_object('button_show_error')
    window.label_status = builder.get_object('label_status')
    window.button_search_adress = builder.get_object('button_search_adress')
    window.button_choose_location = builder.get_object('button_choose_location')
    window.entry_location = builder.get_object('entry_location')
    window.label_time_remaining = builder.get_object('label_time_remaining')
    window.label_download_speed = builder.get_object('label_download_speed')
    window.image_settings_icon = builder.get_object('image_settings_icon')
    window.frame_video_info = builder.get_object('frame_video_info')
    window.frame_subtitles = builder.get_object('frame_subtitles')
    window.box_subtitles = builder.get_object('box_subtitles')
    window.box_video_info = builder.get_object('box_video_info')

    # Set the translated text in the GUI
    window.infobar.button_show_error.set_label(t('button_show_error'))
    window.entry_location.set_text(save_dir)

    # Some objekt don't get a namespace in the window objekt, they are just set once and forgotten about
    builder.get_object('label_entry_description').set_text(t('label_entry_description'))
    builder.get_object('button_search_adress').set_label(t('button_search_adress'))
    builder.get_object('button_choose_location').set_label(t('button_choose_location'))
    builder.get_object('label_title_subtitles').set_text(t('label_title_subtitles'))
    builder.get_object('label_title_video_info').set_text(t('label_title_video_info'))

    try:
        # try to set the settings icon
        image_size = window.image_settings_icon.get_preferred_size().natural_size # get the allocated size from the image widget
        pixbuf_settings_image = GdkPixbuf.Pixbuf().new_from_file_at_size(PIXMAPS_DIR + 'settings-icon.svg', image_size.width, image_size.height) # load the icon with the appropriate size
        window.image_settings_icon.set_from_pixbuf(pixbuf_settings_image)
    except Exception as e:
        print('DEBUG: Could not set icon')
        print(e)

    # Set the title of the window
    window.set_title(t('window_title') + ' - ' + VERSION)
    # Exit the programm, if you close the window
    window.connect('destroy', lambda x: window.close())
    # Show the GUI
    window.show()

if __name__ == '__main__':
    #print('##### Thread: ', threading.get_ident(), ' Location: Hauptprogramm') #For threading tests
    # Change the dir, where the skript resides and init the programm
    os.chdir(EXEC_DIR)
    # Create application
    app = Gtk.Application(application_id=APPLICATION_ID)
    # connect initialize method to the activate signal
    app.connect('activate', initialize)
    # finally run the application
    app.run()
